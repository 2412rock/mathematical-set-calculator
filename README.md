There are four operators that can be used on sets in this language:
+ : A + B = {x|x ∈ A ∨ x ∈ B} union

∗ : A ∗ B = {x|x ∈ A ∧ x ∈ B} intersection

− : A − B = {x|x ∈ A ∧ x /∈ B} complement

| : A | B = {x|x ∈ A + B ∧ x /∈ A ∗ B} symmetric difference
= {x|x ∈ (A + B) − (A ∗ B)}


Example: A − B ∗ Set1 + D is to be evaluated as (A − (B ∗ Set1)) + D.
There are two commands available. For each of those an example is given:
1. Set1 = A + B − Set1 ∗ (D + Set2)
Calculate the set that is the result of the expression to the right of the
’=’-sign and associate the variable with the identifier Set1 with this Set.
2. ? Set1 + Set2
Calculate the set that is the result of the expression, and print the elements
of this set on the standard output.
